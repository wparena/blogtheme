<!-- =========================
     Page Breadcrumb   
============================== -->
<?php get_header(); ?>
<div class="clearfix"></div>
<!-- =========================
     Page Content Section      
============================== -->
 <main id="content">
    <div class="row"> 
      <!--/ Main blog column end -->
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-lg-9 col-md-9"; } ?> content-continer">
          <?php get_template_part('navbar','');?>
          <div class="show-area">
		      <?php if(have_posts())
		        {
		      while(have_posts()) { the_post(); ?>
            <div class="bt-blog-post-box"> <a class="bt-blog-thumb">
			        <?php if(has_post_thumbnail()): ?>
			         <?php $defalt_arg =array('class' => "img-responsive"); ?>
			         <?php the_post_thumbnail('', $defalt_arg); ?>
			        <?php endif; ?></a>

              <article class="small">

                <h1><a><?php the_title(); ?></a></h1>

                <div class="bt-blog-category post-meta-data">

                  <span><?php echo get_the_date( 'F j, Y' ); ?></span>
        
                  | Posted in<a href="#">
                    <?php   $cat_list = get_the_category_list();
                    if(!empty($cat_list)) { ?>
                    <?php the_category(', '); ?>
                  </a>
                  <?php } ?>
                </div>
                <hr>
                <?php the_content(); ?>
              </article>
              <?php wp_link_pages( array( 'before' => '<div class="link single-page-break">' . __( 'Pages:', 'blogtheme' ), 'after' => '</div>' ) ); ?>
            </div>
		      <?php } ?>
            <div class="media bt-info-author-block"> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>" class="bt-author-pic"> <?php echo get_avatar( get_the_author_meta( 'ID') , 150); ?> </a>
              <div class="media-body">
                <h4 class="media-heading"><?php the_author_posts_link(); ?></h4>
                <p><?php the_author_meta( 'description' ); ?></p>
                <div class="row">
                  <div class="col-md-6 col-pad7">
                    <ul class="list-inline info-author-social">
          					<?php 
          					$facebook_profile = get_the_author_meta( 'facebook_profile' );
          					if ( $facebook_profile && $facebook_profile != '' ) {
          					echo '<li class="facebook"><a href="' . esc_url($facebook_profile) . '"><i class="fa fa-facebook-square"></i></a></li>';
          					} 
					
          					$twitter_profile = get_the_author_meta( 'twitter_profile' );
          					if ( $twitter_profile && $twitter_profile != '' ) 
          					{
          					echo '<li class="twitter"><a href="' . esc_url($twitter_profile) . '"><i class="fa fa-twitter-square"></i></a></li>';
          					}
					
          					$google_profile = get_the_author_meta( 'google_profile' );
          					if ( $google_profile && $google_profile != '' ) {
          					echo '<li class="googleplus"><a href="' . esc_url($google_profile) . '" rel="author"><i class="fa fa-google-plus-square"></i></a></li>';
          					}
          					$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
          					if ( $linkedin_profile && $linkedin_profile != '' ) {
          					   echo '<li class="linkedin"><a href="' . esc_url($linkedin_profile) . '"><i class="fa fa-linkedin-square"></i></a></li>';
          					}
          					?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
		      <?php } ?>
         <?php comments_template('',true); ?>
       </div>
      </div>
      <div class="col-md-3 col-lg-3">
      <?php get_sidebar(); ?>
      </div>
    </div>
    <!--/ Row end -->
</main>
<?php get_footer(); ?>