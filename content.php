<?php 
/**
 * The template for displaying the content.
 * @package blogtheme
 */
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="bt-blog-post-box">
				<?php
					if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
						if ((has_post_thumbnail()) || (!has_post_thumbnail() && !is_single())) :
							?>

							<div class="show-date">
								<span><?php echo get_the_date( 'm/j' ); ?></span><br>
								<span class="year"><?php echo get_the_date( 'Y' ); ?></span>
							</div>

							<div class="comments-count">
								<a href="<?php esc_url(comments_link()); ?>">
									<?php
									echo '<span class="comments-num">'.number_format_i18n(get_comments_number()).'</span><span class="comments-typo">Comments</span>';
									?>
								</a>
							</div>
							<?php
						endif;
					endif;
				?>
			<div class="row">
			<?php if(has_post_thumbnail()):?>
				<div class="col-md-4 col-sm-4 col-lg-4">
					<div class="small-img">
						<a href="<?php the_permalink(); ?>" class="bt-blog-thumb">
							<?php if(has_post_thumbnail()): ?>
								<?php $defalt_arg =array('class' => "img-responsive"); ?>
								<?php the_post_thumbnail('', $defalt_arg); ?>
							<?php endif; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>


			<article class="small col-md-8 col-sm-8 col-lg-8">
				<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h1>
				<div class="bt-blog-category post-meta-data"> 

					<div class="post-meta">
						Posted In:
						<a href="#">
						 	<?php   $cat_list = get_the_category_list();
						  	if(!empty($cat_list)) { ?>
						  	<?php the_category(', '); ?>
						</a>
						<?php } ?>
					</div>
				</div>
				<p>
					<?php
						echo get_the_excerpt();
					?>
				</p>
					<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'blogtheme' ), 'after' => '</div>' ) ); ?>
			</article>
		</div>
		</div>
	</div>
