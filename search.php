<?php
/**
 * The template for displaying search results pages.
 *
 * @package blogtheme
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
    <div class="row">
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-lg-9 col-md-9"; } ?> content-continer">
		<?php get_template_part('navbar','');?>
			<div class="show-area">
		        <?php 
				global $i;
				if ( have_posts() ) : ?>
				<h2><?php printf( __( "Search Results for: %s", 'blogtheme' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
				<br>
				<?php while ( have_posts() ) : the_post();  
				 get_template_part('content','');
				 endwhile; else : ?>
				<h2><?php _e('Not Found','blogtheme'); ?></h2>
				<div class="">
				<p><?php _e('Sorry, Do Not match.','blogtheme' ); ?>
				</p>
				<?php get_search_form(); ?>
				</div><!-- .blog_con_mn -->
				<?php endif; ?>
			</div>
      </div>
	  <div class="col-md-3 col-lg-3">
        <?php get_sidebar(); ?>
      </div>
    </div>
</main>
<?php
get_footer();
?>