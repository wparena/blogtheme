<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package blogtheme
 */
get_header(); ?>
<main id="content">
		<!-- Blog Area -->
	<div class="row">
		<div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-lg-9 col-md-9"; } ?> content-continer">
				<?php get_template_part('navbar','');?>
        	<div class="show-area">
        		<h2><?php the_title(); ?></h2>
				<?php if( have_posts()) :  the_post(); ?>		
				<?php the_content(); ?>
				<?php endif; ?>
				<?php wp_link_pages( array( 'before' => '<div class="link page-break-links">' . __( 'Pages:', 'blogtheme' ), 'after' => '</div>' ) ); ?>
				<?php comments_template( '', true ); // show comments ?>
			<!-- /Blog Area -->	
			</div>		
		</div>
			<!--Sidebar Area-->
			<aside class="col-md-3 col-lg-3">
				<?php get_sidebar(); ?>
			</aside>
			<!--Sidebar Area-->
	</div>
</main>
<?php
get_footer();