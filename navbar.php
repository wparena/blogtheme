    <div class="nav-container">
      <nav class="navbar navbar-default navbar-static-top navbar-wp">
            <!-- navbar-toggle -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-wp"> <span class="sr-only"><?php __('Toggle Navigation','blogtheme'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <!-- /navbar-toggle --> 
            <!-- Navigation -->
            
            <div class="collapse navbar-collapse" id="navbar-wp">
              <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'blogtheme_custom_navwalker::fallback' , 'walker' => new blogtheme_custom_navwalker() ) ); ?>

            </div>
            <!-- /Navigation -->
        </nav>
      </div>