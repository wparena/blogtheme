=== BlogTheme ===

Contributors: wpcount
Requires at least: WordPress 4.7
Tested up to: WordPress 5.0-trunk
Version: 2.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, custom-logo, sticky-post, flexible-header, custom-menu, featured-images, footer-widgets, blog, threaded-comments, translation-ready

== Description ==

BlogTheme is a simple WordPress Theme suitable for any blog/magazine type. The theme is simple but yet elegant and easy-to-use. Theme is a flexible foundation that adapts your website to mobile devices and the desktop or any other viewing environment. Theme is built with lightweight HTML5 and latest CSS. Features include a homepage template with blog posts, two widget areas and two menus places.
BlogTheme is Created with Twitter Bootstrap 3.3.7 Framework. BlogTheme is a great design idea for
websites like Personal Portfolio, Blog and Magzine. Create Outstanding Website or Blog in Minutes!.

Theme has two layouts. A "single column layout" and a "two columns layout". The content is on left side and a right sidebar widget. We focused on usability across various devices, starting with smart phones.it is compatible with various devices. BlogTheme is a Cross-Browser Compatible theme that works on All leading web browsers. BlogTheme is easy to use and 
user friendly theme.

To make your site attractive it has two widget sections first for right-sidebar widget section and 
second for Footer widget sections.

To make your website in two columns use sidebar widget section. To set custom menu in header set primary location. We also added social media links to add your social links.It boasts of beautifully designed page sections , Home, Blog and Default Page Template(page with right sidebar). BlogTheme is translation ready theme with WPML compatible & Many More.

This theme is compatible with Wordpress Version 4.7  and above and it supports the new theme customization API (https://codex.wordpress.org/Theme_Customization_API).

Supported browsers: Firefox, Opera, Chrome, Safari and IE11 (Some css3 styles like shadows, rounder corners and 2D transform are not supported by IE8 and below).

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.

2. Type in BlogTheme in the search form and press the 'Enter' key on your keyboard.

3. Click on the 'Activate' button to use your new theme right away.

4. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

BlogTheme WordPress Theme, Copyright 2018
BlogTheme is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

BlogTheme bundles the following third-party resources:

======================================
This theme uses Bootstrap as a design tool
======================================
* Bootstrap (http://getbootstrap.com/)
* Copyright (c) 2011-2016 Twitter, Inc
* Licensed under https://github.com/twbs/bootstrap/blob/master/LICENSE

============================================
This theme uses Font Awesome for the theme icons
============================================
* Font Awesome icons, Copyright Dave Gandy
* License: SIL Open Font License, version 1.1.
* Source: http://fontawesome.io/

======================================
This theme uses daneden/animate
======================================
* Animate  : https://github.com/daneden
* Copyright (c) 2018 Daniel Eden ( https://github.com/daneden/animate.css/blob/master/LICENSE )
* Licensed under https://github.com/daneden/animate.css

== Changelog ==

= 2.0.3 =

* Released: March 19, 2018
* Some CSS Changes
* Reduce Js Files By Combining

= 2.0.2 =

* Released: March 14, 2018
* Removal Of Bugs/Errors

= 2.0.1 =

* Released: Feb 24, 2018
* Redesign Structure Of Theme

= 2.0.0 =

* Released: Feb 13, 2018
* Addition Of CSS Rules
* Basic Changes In Theme's Layout

= 1.0.0 =

* Released: Jan 10, 2009

Initial release